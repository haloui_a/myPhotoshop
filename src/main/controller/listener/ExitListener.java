package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 13/05/16.
 */
public class ExitListener extends DefaultListener implements ActionListener {
    public ExitListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!appModel.areProjectsSaved()) {
            int confirm = JOptionPane.showOptionDialog(null,
                    "Warning : some of your projects are not saved, would you like to save them ?",
                    "Confirm", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, null, null);
            if (confirm == JOptionPane.NO_OPTION) {
                System.exit(0);
            } else if (confirm == JOptionPane.YES_OPTION) {
                appModel.saveAllProjects();
                System.exit(0);
            }
        } else {
            System.exit(0);
        }
    }
}

package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created by root on 11/05/16.
 */
public class ProjectSwitchListener extends DefaultListener implements ChangeListener {
    public ProjectSwitchListener(AppModel appModel) {
        super(appModel);
    }


    @Override
    public void stateChanged(ChangeEvent e) {
        JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
        int index = sourceTabbedPane.getSelectedIndex();
        appModel.setCurrentProject(index);
    }
}

package main.view.toolbar;

import main.view.project.PanelView;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class ToolBarView extends PanelView {
    private JToolBar toolBar;
    private ImageIcon loadingIcon = new ImageIcon("icons/loading1.gif");
    private ImageIcon loadingPressIcon = new ImageIcon("icons/loadingPress1.png");

    public ToolBarView() {
        toolBar = new JToolBar("Toolbar");
        init();
        toolBar.addSeparator();
        toolBar.setOrientation(SwingConstants.VERTICAL);
        this.add(toolBar);
        this.revalidate();
        this.repaint();
        toolBar.revalidate();
        toolBar.repaint();
    }

    protected ButtonView createButton(String imagePath1, String imagePath2, String name, ActionListener listener) {
        ImageIcon imageIcon1 = new ImageIcon("icons/" + imagePath1);
        ImageIcon imageIcon2 = new ImageIcon("icons/" + imagePath2);
        ButtonView button = new ButtonView(imageIcon1, imageIcon2, loadingIcon, loadingPressIcon);
        button.setPressedIcon(imageIcon2);
        button.addActionListener(listener);
        button.setName(name);
        appController.addObserver(button);
        return button;
    }

    public void init() {
        toolBar.add(createButton("next1.png", "next2.png", "REDO", appController.GetButtonListener()));
        toolBar.add(createButton("previous1.png", "previous2.png", "UNDO", appController.GetButtonListener()));
        toolBar.add(createButton("filter1.png", "filter2.png", "FILTER", appController.GetButtonListener()));
    }
}

package main.controller.listener;

import main.model.AppModel;
import main.model.history.HistoryModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class RedoListener extends DefaultListener implements ActionListener {
    public RedoListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        HistoryModel history = appModel.getCurrentProject().getHistory();
        appModel.historySelectItem(Integer.min(history.getCurrentPosition() + 1, history.getList().size() - 1));
    }
}

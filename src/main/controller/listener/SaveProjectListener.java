package main.controller.listener;

import main.model.AppModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class SaveProjectListener extends DefaultListener implements ActionListener {
    public SaveProjectListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appModel.SaveCurrentProject();
    }
}

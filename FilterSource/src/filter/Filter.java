package filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 11/05/16.
 */
public interface Filter {
    public BufferedImage perform(BufferedImage img);
    String getName();
}

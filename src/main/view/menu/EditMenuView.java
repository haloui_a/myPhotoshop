package main.view.menu;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Created by root on 11/05/16.
 */
public class EditMenuView extends MenuView {

    public EditMenuView() {
        super();
        this.setText(language.GetTitle(language.TITLE_EDIT));
        this.setMnemonic(KeyEvent.VK_E);
        JMenuItem menuUndoItem = new JMenuItem(language.GetTitle(language.TITLE_UNDO), KeyEvent.VK_U);
        JMenuItem menuRedoItem = new JMenuItem(language.GetTitle(language.TITLE_REDO), KeyEvent.VK_R);
        JMenuItem menuRenameItem = new JMenuItem(language.GetTitle(language.TITLE_RENAME_PROJECT));
        JMenuItem menuChangeHistorySizeItem = new JMenuItem("Change history size");

        menuUndoItem.addActionListener(appController.GetUndoListener());
        menuRedoItem.addActionListener(appController.GetRedoListener());
        menuRenameItem.addActionListener(appController.GetRenameListener());
        menuChangeHistorySizeItem.addActionListener(appController.GetChangeHistorySizeListener());

        this.add(menuUndoItem);
        this.add(menuRedoItem);
        this.addSeparator();
        this.add(menuRenameItem);
        this.add(menuChangeHistorySizeItem);
    }
}

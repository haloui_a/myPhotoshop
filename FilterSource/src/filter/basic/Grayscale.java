package filter.basic;

import filter.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class Grayscale implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        BufferedImage res = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++) {
                Color c = new Color(img.getRGB(i, j));
                int k = (c.getRed() + c.getBlue() + c.getGreen()) / 3;
                res.setRGB(i, j, new Color(k, k, k).getRGB());
            }
        return res;
    }

    @Override
    public String getName()
    {
        return "Grayscale";
    }
}

package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class SaveProjectAsListener extends DefaultListener implements ActionListener {
    public SaveProjectAsListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser c = new JFileChooser();
        int rVal = c.showSaveDialog(null);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            appModel.SaveCurrentProjectAs(c.getSelectedFile().getPath() + ".psd");
        }
    }

    public void start() {
        JFileChooser c = new JFileChooser();
        int rVal = c.showSaveDialog(null);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            appModel.SaveCurrentProjectAs(c.getSelectedFile().getPath() + ".psd");
        }
    }
}

package main.view.menu;

import main.config.Language;
import main.controller.AppController;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by root on 12/05/16.
 */
public class MenuView extends JMenu implements Observer {
    protected Language language = Language.getInstance();
    AppController appController = AppController.getInstance();

    public MenuView() {
        super();
        appController.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}

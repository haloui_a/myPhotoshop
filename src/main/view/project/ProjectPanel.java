package main.view.project;

import main.controller.AppController;
import main.model.ProjectModel;

import javax.swing.*;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by root on 11/05/16.
 */
public class ProjectPanel extends JScrollPane implements Observer {
    private ImageView imageView;
    private JLabel imageLabel;
    private ImageIcon imageIcon;
    private AppController appController = AppController.getInstance();
    private boolean init = false;

    public ProjectPanel() {
        super();
        appController.addObserver(this);
    }

    public void loadImage(File file) {
        imageView = new ImageView(file);
        this.add(imageView);
    }

    public void loadImage(ImageView image) {
        imageView = image;
        imageView.buildImage();
        this.add(imageView);
        imageIcon = new ImageIcon(imageView.getImage());
        if (init == false) {
            imageLabel = new JLabel();
            this.getViewport().add(imageLabel);
            init = true;
        }
        imageLabel.setIcon(imageIcon);
        imageLabel.revalidate();
        imageLabel.repaint();
        revalidate();
        repaint();
    }

    @Override
    public void update(Observable obs, Object obj) {
        if (obs instanceof ProjectModel && obj instanceof String) {
            if ((String) obj == "SaveImage")
                appController.GetSaveImageAsListener().start();
            else if ((String) obj == "SaveProject")
                appController.GetSaveProjectAsListener().start();
        } else {

            if (obs instanceof ProjectModel && ((ProjectModel) obs).GetImage().getImage() != null) {
                imageView = ((ProjectModel) obs).GetImage();
                imageIcon = new ImageIcon(imageView.getImage());
                if (init == false) {
                    imageLabel = new JLabel();
                    this.getViewport().add(imageLabel);
                    init = true;
                }
                imageLabel.setIcon(imageIcon);
                imageLabel.revalidate();
                imageLabel.repaint();
                revalidate();
                repaint();
            }
        }
    }
}

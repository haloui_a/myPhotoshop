package main.controller.listener;

import main.model.AppModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 14/05/16.
 */
public class SaveAllListener extends DefaultListener implements ActionListener {
    public SaveAllListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appModel.saveAllProjects();
    }
}

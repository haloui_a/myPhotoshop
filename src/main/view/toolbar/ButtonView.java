package main.view.toolbar;

import main.model.AppModel;
import main.model.ProjectModel;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by root on 13/05/16.
 */
public class ButtonView extends JButton implements Observer {
    private ImageIcon icon1;
    private ImageIcon icon2;
    private ImageIcon icon3;
    private ImageIcon icon4;

    public ButtonView(ImageIcon icon1, ImageIcon icon2, ImageIcon icon3, ImageIcon icon4) {
        super(icon1);
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.icon3 = icon3;
        this.icon4 = icon4;
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof AppModel && arg instanceof Integer) {
            if (this.getName() == ((AppModel) o).getLastClicked()) {
                switch ((Integer) arg) {
                    case 1:
                        this.setIcon(icon1);
                        this.setPressedIcon(icon2);
                        break;
                    case 2:
                        this.setIcon(icon3);
                        this.setPressedIcon(icon4);
                        break;
                    default:
                        break;
                }
            }
            this.revalidate();
            this.repaint();
        }
        if (o instanceof ProjectModel && arg instanceof Integer) {
            switch ((Integer) arg) {
                case 1:
                    this.setIcon(icon1);
                    this.setPressedIcon(icon2);
                    break;
                case 2:
                    this.setIcon(icon3);
                    this.setPressedIcon(icon4);
                    break;
                default:
                    break;
            }
        }
    }
}

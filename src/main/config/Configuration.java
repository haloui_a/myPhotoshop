package main.config;

import java.awt.*;

/**
 * Created by root on 11/05/16.
 */
public class Configuration {
    public static final int INTITIAL_HEIGHT = 1080;
    public static final int INITIAL_WIDTH = 1920;
    public static final boolean RESIZABLE = true;
    public static final String TITLE = "MyPhotoshop";
    public static final Font DEFAULT_FONT = new Font("helvitica", Font.BOLD, 24);
}

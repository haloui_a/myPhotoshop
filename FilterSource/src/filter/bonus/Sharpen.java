package filter.bonus;

import filter.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 14/05/16.
 */
public class Sharpen extends Convolution implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        double[][] kernel = {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}};
        BufferedImage res = performConvolution(kernel, img);
        return res;
    }

    @Override
    public String getName()
    {
        return "Sharpen";
    }
}

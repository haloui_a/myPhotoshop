package main.controller.listener;

import main.model.AppModel;
import main.model.history.HistoryModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class UndoListener extends DefaultListener implements ActionListener {
    public UndoListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        HistoryModel history = appModel.getCurrentProject().getHistory();
        appModel.historySelectItem(Integer.max(0, history.getCurrentPosition() - 1));
    }
}

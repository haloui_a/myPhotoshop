package main.filter;

import filter.Filter;
import main.model.ProjectModel;

import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class ThreadedFilter implements Runnable {
    private ProjectModel projectModel = null;
    private boolean stopped = false;

    public ThreadedFilter(ProjectModel projectModel) {
        this.projectModel = projectModel;
    }

    public void stop() {
        this.stopped = true;
    }

    public void run() {
        while (this.projectModel.getFiltersQueue().size() > 0 && stopped == false) {
            Filter filter = this.projectModel.getFiltersQueue().get(0);
            this.projectModel.isFilterRunning = true;
            BufferedImage newImage = filter.perform(this.projectModel.GetImage().getImage());
            this.projectModel.reloadImage(newImage);
            this.projectModel.getHistory().Add(filter.getName(), this.projectModel.GetImage());
            this.projectModel.setModified();
            this.projectModel.getFiltersQueue().remove(0);
            this.projectModel.isFilterRunning = false;
        }
        projectModel.stopThread();
    }
}

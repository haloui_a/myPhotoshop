package filter.bonus;

import filter.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class Blur extends Convolution implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        double[][] kernel = {{1/9f, 1/9f, 1/9f}, {1/9f, 1/9f, 1/9f}, {1/9f, 1/9f, 1/9f}};
        BufferedImage res = performConvolution(kernel, img);
        return res;
    }

    @Override
    public String getName()
    {
        return "Blur";
    }
}

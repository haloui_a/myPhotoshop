package main.model;

import filter.Filter;
import main.config.Language;
import main.filter.ThreadedFilter;
import main.model.history.HistoryModel;
import main.view.project.ImageView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;

/**
 * Created by root on 11/05/16.
 */
public class ProjectModel extends ModelObject implements Serializable {
    protected transient Language language = Language.getInstance();

    private ImageView image;
    private String title = null;
    private String projectPath = null;
    private String imagePath = null;
    private String imageType = null;
    private HistoryModel history;
    private ArrayList<Filter> filtersQueue = new ArrayList<>();
    private boolean saved = false;
    public boolean isFilterRunning = false;
    private Thread thread = null;

    public ArrayList<Filter> getFiltersQueue() {
        return filtersQueue;
    }

    public void stopThread() {
        filtersQueue.clear();
        this.update(1);
        thread.interrupt();
        thread.stop();
        thread = null;
    }

    public void applyFilter(Filter filter) {
        filtersQueue.add(filter);
        if (thread == null) {
            ThreadedFilter filterThread = new ThreadedFilter(this);
            thread = new Thread(filterThread);
            thread.start();
        }
    }

    public void setHistorySize(int i) {
        history.setSize(i);
    }

    public void setHistory(HistoryModel history) {
        this.history = history;
    }

    public HistoryModel getHistory() {
        return history;
    }

    public void SaveProjectAs(String location) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(location), false);
            GZIPOutputStream gzipos = new GZIPOutputStream(fos) {

                {
                    def.setLevel(Deflater.BEST_COMPRESSION);
                }
            };
            projectPath = location;
            ObjectOutputStream oosCompressed = new ObjectOutputStream(gzipos);
            oosCompressed.writeObject(this);
            oosCompressed.flush();
            oosCompressed.close();
            setSaved();
        } catch (Exception e) {
            e.printStackTrace();
        }
        saved = true;
    }

    private void setSaved() {
        saved = true;
    }

    public void setModified() {
        saved = false;
    }

    public void SaveProject() {
        if (saved == false) {
            if (projectPath != null)
                SaveProjectAs(projectPath);
            else
                update("SaveProject");
        }
        saved = true;
    }

    public ProjectModel() {
        super();
    }

    public ProjectModel(String title) {
        super();
        this.title = title;
    }

    public void loadImage(File file) {
        image = new ImageView(file);
        update();
        history.Add(language.GetTitle(language.TITLE_LOAD_IMAGE), image);
        setModified();
    }

    public void reloadImage(BufferedImage image) {
        this.image.setImage(image);
        update();
        setModified();
    }

    public void saveImage() {
        if (imagePath != null)
            saveImageAs(imagePath, imageType);
        else
            update("SaveImage");
    }

    public void saveImageAs(String location, String type) {
        File outputfile = new File(location);
        try {
            ImageIO.write(this.GetImage().getImage(), type, outputfile);
            imagePath = location;
            imageType = type;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ImageView GetImage() {
        return image;
    }

    public void update(String state) {
        setChanged();
        notifyObservers(state);
    }

    @Override
    public void update() {
        setChanged();
        notifyObservers(this);
        setModified();
    }

    public void update(Integer i) {
        setChanged();
        notifyObservers(i);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSaved() {
        return saved;
    }
}

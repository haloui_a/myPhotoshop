package main.view.history;

import main.model.AppModel;
import main.model.StatedObject;
import main.model.history.HistoryModel;
import main.view.project.PanelView;

import javax.swing.*;
import java.util.Observable;

/**
 * Created by root on 11/05/16.
 */
public class HistoryPanelView extends PanelView {
    private JList historyList = null;
    private boolean init = false;

    public HistoryPanelView() {
        appController.addObserver(this);
    }

    @Override
    public void update(Observable obs, Object obj) {
        if (obs instanceof HistoryModel && obj instanceof StatedObject && ((StatedObject) obj).getType() == HistoryModel.class && ((StatedObject) obj).getState() == "ADD") {
            HistoryModel historyModel = ((HistoryModel) ((StatedObject) obj).getObject());
            Object[] strList = historyModel.getList().toArray();
            JList newHistoryList = new JList(strList);
            newHistoryList.setCellRenderer(new IconListRenderer(historyModel));
            newHistoryList.setSelectedIndex(historyModel.getCurrentPosition());
            if (init == false) {
                historyList = newHistoryList;
                historyList.addListSelectionListener(appController.GetSelectHistoryItemListener());
                this.add(new JScrollPane(historyList));
                init = true;
            }
            historyList.setListData(strList);
            historyList.setCellRenderer(new IconListRenderer(historyModel));
            historyList.setSelectedIndex(historyModel.getCurrentPosition());
            historyList.revalidate();
            historyList.repaint();
            this.revalidate();
            this.repaint();
        }

        if (obs instanceof AppModel && obj instanceof StatedObject && ((StatedObject) obj).getType() == HistoryModel.class && ((StatedObject) obj).getState() == "SWITCH") {
            HistoryModel historyModel = ((HistoryModel) ((StatedObject) obj).getObject());
            historyModel.load();
            Object[] strList = historyModel.getList().toArray();
            JList newHistoryList = new JList(strList);
            newHistoryList.setCellRenderer(new IconListRenderer(historyModel));
            newHistoryList.setSelectedIndex(historyModel.getCurrentPosition());
            newHistoryList.setListData(strList);
            if (init == false) {
                historyList = newHistoryList;
                historyList.addListSelectionListener(appController.GetSelectHistoryItemListener());
                this.add(new JScrollPane(historyList));
                init = true;
            }
            historyList.setListData(strList);
            historyList.setCellRenderer(new IconListRenderer(historyModel));
            historyList.setSelectedIndex(historyModel.getCurrentPosition());
            historyList.revalidate();
            historyList.repaint();
            this.revalidate();
            this.repaint();
        }

        if (obs instanceof HistoryModel && obj instanceof StatedObject && ((StatedObject) obj).getType() == HistoryModel.class && ((StatedObject) obj).getState() == "SWITCH") {
            HistoryModel historyModel = ((HistoryModel) ((StatedObject) obj).getObject());
            historyList.setSelectedIndex(historyModel.getCurrentPosition());
            historyList.revalidate();
            historyList.repaint();
            this.revalidate();
            this.repaint();
        }
    }
}

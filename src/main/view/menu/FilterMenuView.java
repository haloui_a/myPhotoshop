package main.view.menu;

import main.model.AppModel;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.Observable;

/**
 * Created by root on 11/05/16.
 */
public class FilterMenuView extends MenuView {
    public FilterMenuView() {
        super();
        this.setText(language.GetTitle(language.TITLE_FILTERS));
        this.setMnemonic(KeyEvent.VK_F);
        this.addSeparator();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof AppModel && arg instanceof String[]) {
            String[] filters = (String[]) arg;
            for (String filter : filters) {
                JMenuItem newFilterItem = new JMenuItem(filter);
                newFilterItem.addActionListener(appController.GetFilterListener());
                this.add(newFilterItem);
            }
            this.addSeparator();
        }
    }
}

package main.model;

import java.lang.reflect.Type;

/**
 * Created by root on 11/05/16.
 */
public class StatedObject<T extends ModelObject> {
    private String state;
    private T object;

    public StatedObject(T object, String state) {
        this.object = object;
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public T getObject() {
        return object;
    }

    public Type getType() {
        return object.getClass();
    }
}

package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class RenameListener extends DefaultListener implements ActionListener {
    public RenameListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = JOptionPane.showInputDialog(null, language.GetTitle(language.TITLE_TYPE_PROJECT_NAME));
        appModel.renameCurrentProject(name);
    }
}

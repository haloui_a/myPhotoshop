package main.model.history;

import main.model.ProjectModel;
import main.view.project.ImageView;

import java.io.Serializable;

/**
 * Created by root on 11/05/16.
 */
public class HistoryItem extends ProjectModel implements Serializable {
    private String description;
    private ImageView image;

    public void load() {
        image.buildImage();
    }

    public HistoryItem(String title, String description, ImageView image) {
        super(title);
        this.description = description;
        this.image = image;
    }

    public HistoryItem(String description, ImageView image) {
        this.description = description;
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public ImageView getImage() {
        return image;
    }
}

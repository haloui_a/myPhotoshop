package main.controller.listener;

import main.model.AppModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 11/05/16.
 */
public class NewProjectListener extends DefaultListener implements ActionListener {
    public NewProjectListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appModel.newProject();
    }
}

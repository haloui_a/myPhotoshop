package filter.bonus;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class Convolution {
    public BufferedImage performConvolution(double[][] kernel, BufferedImage img) {
        BufferedImage newImg = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        int r, g, b;
        for (int i = 0; i < img.getWidth(); ++i) {
            for (int j = 0; j < img.getHeight(); ++j) {
                boolean outOfSight = false;
                r = 0;
                g = 0;
                b = 0;
                for (int x = 0; x < 3; ++x) {
                    for (int y = 0; y < 3; ++y) {
                        int tI = i - 1 + y;
                        int tJ = j - 1 + x;
                        if (inBound(img, tI, tJ)) {
                            r += Math.round((double) new Color(img.getRGB(tI, tJ)).getRed() * kernel[x][y]);
                            g += Math.round((double) new Color(img.getRGB(tI, tJ)).getGreen() * kernel[x][y]);
                            b += Math.round((double) new Color(img.getRGB(tI, tJ)).getBlue() * kernel[x][y]);
                        }
                    }
                }
                if (outOfSight)
                    newImg.setRGB(i, j, (img.getRGB(i, j)));
                else
                    newImg.setRGB(i, j, (new Color(truncateColor(r),  truncateColor(g),  truncateColor(b)).getRGB()));
            }
        }
        return newImg;
    }

    public int truncateColor(double colorComp) {
        if (colorComp >= 255)
            return 255;
        else if (colorComp < 0)
            return 0;
        else
            return (int) colorComp;
    }

    public boolean inBound(BufferedImage img, int tI, int tJ) {
        return (tI > 0 && tJ > 0 && tI < img.getWidth() && tJ < img.getHeight());
    }
}

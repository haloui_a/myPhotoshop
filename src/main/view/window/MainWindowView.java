package main.view.window;

import main.config.Language;
import main.model.AppModel;
import main.model.ProjectModel;
import main.model.StatedObject;
import main.view.history.HistoryPanelView;
import main.view.menu.TopMenuView;
import main.view.project.PanelView;
import main.view.project.ProjectPanel;
import main.view.toolbar.ToolBarView;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by root on 11/05/16.
 */
@SuppressWarnings("AccessStaticViaInstance")
public class MainWindowView extends WindowView implements Observer {
    protected Language language = Language.getInstance();

    private JPanel panel;
    private JTabbedPane tabbedPane;
    private TopMenuView topMenu;
    private HistoryPanelView historyPanelView;
    private ToolBarView toolBarView;

    public MainWindowView(String title) {
        super(title);
        setPanel();
        setMenu();
        appController.addObserver(this);
        appController.initView();
    }

    public MainWindowView() {
        super();
        setPanel();
        setMenu();
        appController.addObserver(this);
        appController.initView();
    }

    private void setMenu() {
        topMenu = new TopMenuView();
        this.setJMenuBar(topMenu);
    }

    private void setPanel() {
        panel = new PanelView();
        getContentPane().add(panel);
        tabbedPane = new JTabbedPane();
        historyPanelView = new HistoryPanelView();
        toolBarView = new ToolBarView();
        panel.add(tabbedPane, BorderLayout.CENTER);
        panel.add(historyPanelView, BorderLayout.EAST);
        panel.add(toolBarView, BorderLayout.WEST);
        tabbedPane.addChangeListener(appController.GetProjectSwitchListener());
    }

    public void addTab(String title, ProjectPanel projectPanel) {
        tabbedPane.addTab(title, projectPanel);
    }

    @Override
    public void update(Observable obs, Object obj) {
        if (obs instanceof AppModel && obj instanceof StatedObject && ((StatedObject) obj).getState() == "RENAME") {
            ProjectModel projectModel = (ProjectModel) ((StatedObject) obj).getObject();
            tabbedPane.getSelectedComponent().setName(projectModel.getTitle());
            tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), projectModel.getTitle());
            tabbedPane.revalidate();
            tabbedPane.repaint();
            this.refresh();
        }

        if (obs instanceof AppModel && obj instanceof StatedObject && ((StatedObject) obj).getState() == "CREATE") {
            String name = JOptionPane.showInputDialog(this, language.GetTitle(language.TITLE_TYPE_PROJECT_NAME));
            ProjectPanel projectPanel = new ProjectPanel();
            addTab(name, projectPanel);
            appController.addObserver(historyPanelView);
            appController.setProjectName(name);
            tabbedPane.setSelectedComponent(projectPanel);
            this.refresh();
        }

        if (obs instanceof AppModel && obj instanceof StatedObject && ((StatedObject) obj).getState() == "OPEN") {
            ProjectPanel projectPanel = new ProjectPanel();
            ProjectModel projectModel = (ProjectModel) ((StatedObject) obj).getObject();
            projectPanel.loadImage(projectModel.GetImage());
            addTab(projectModel.getTitle(), projectPanel);
            appController.addObserver(historyPanelView);
            appController.setProjectName(projectModel.getTitle());
            tabbedPane.setSelectedComponent(projectPanel);
            projectPanel.loadImage(projectModel.GetImage());
            this.refresh();
        }

        if (obs instanceof AppModel && obj instanceof StatedObject && ((StatedObject) obj).getState() == "CLOSE") {
            tabbedPane.remove(((AppModel) obs).getCurrentProjectId());
            this.refresh();
        }

    }
}

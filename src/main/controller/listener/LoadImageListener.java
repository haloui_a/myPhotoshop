package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by root on 11/05/16.
 */
public class LoadImageListener extends DefaultListener implements ActionListener {
    public LoadImageListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg", "bmp");
        fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            appModel.getCurrentProject().loadImage(file);
        }
    }
}

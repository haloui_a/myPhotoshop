package filter.basic;

import filter.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class VerticalFlip implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        BufferedImage res = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++) {
                int k = i + 1;
                res.setRGB(img.getWidth() - k, j, img.getRGB(i, j));
            }
        return res;
    }

    @Override
    public String getName()
    {
        return "VerticalFlip";
    }
}

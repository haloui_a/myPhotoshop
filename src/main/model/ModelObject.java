package main.model;

import java.util.Observable;

/**
 * Created by root on 11/05/16.
 */
public class ModelObject extends Observable {

    public ModelObject() {
        super();
    }

    public void update() {
        setChanged();
        notifyObservers(this);
    }
}

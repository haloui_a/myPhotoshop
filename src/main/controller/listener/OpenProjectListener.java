package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class OpenProjectListener extends DefaultListener implements ActionListener {
    public OpenProjectListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Project Files", "psd", "PSD");
        fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        int rVal = fc.showOpenDialog(null);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            appModel.openProject(fc.getSelectedFile().getPath());
        }
    }
}

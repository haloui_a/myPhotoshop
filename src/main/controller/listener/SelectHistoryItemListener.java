package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Created by root on 12/05/16.
 */
public class SelectHistoryItemListener extends DefaultListener implements ListSelectionListener {

    public SelectHistoryItemListener(AppModel appModel) {
        super(appModel);
    }

    public void valueChanged(ListSelectionEvent e) {
        JList lsm = (JList) e.getSource();

        int selectedIndex = lsm.getSelectedIndex();
        appModel.historySelectItem(selectedIndex);
    }
}

package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 12/05/16.
 */
public class SaveImageAsListener extends DefaultListener implements ActionListener {
    public SaveImageAsListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        start();
    }

    public void start() {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "gif", "bmp");
        fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        int rVal = fc.showSaveDialog(null);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            String ext = fc.getSelectedFile().getPath().substring(fc.getSelectedFile().getPath().lastIndexOf('.') + 1).toLowerCase();
            switch (ext) {
                case "jpg":
                    appModel.getCurrentProject().saveImageAs(fc.getSelectedFile().getPath(), "jpg");
                    break;
                case "png":
                    appModel.getCurrentProject().saveImageAs(fc.getSelectedFile().getPath(), "png");
                    break;
                case "gif":
                    appModel.getCurrentProject().saveImageAs(fc.getSelectedFile().getPath(), "gif");
                    break;
                case "bmp":
                    appModel.getCurrentProject().saveImageAs(fc.getSelectedFile().getPath(), "bmp");
                    break;
                default:
                    appModel.getCurrentProject().saveImageAs(fc.getSelectedFile().getPath() + ".png", "png");
                    break;
            }
        }
    }
}

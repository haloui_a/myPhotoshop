package filter.bonus;

import filter.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 14/05/16.
 */
public class EdgeDetection extends Convolution implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        double[][] kernel = {{0, 1, 0}, {1, -4, 1}, {0, 1, 0}};
        BufferedImage res = performConvolution(kernel, img);
        return res;
    }

    @Override
    public String getName()
    {
        return "EdgeDetection";
    }
}

package main.controller;

import filter.Filter;
import main.config.Language;
import main.controller.listener.*;
import main.filter.JarLoader;
import main.model.AppModel;
import main.view.history.HistoryPanelView;
import main.view.menu.MenuView;
import main.view.project.ProjectPanel;
import main.view.toolbar.ButtonView;
import main.view.window.MainWindowView;

import javax.swing.*;

/**
 * Created by root on 11/05/16.
 */
public class AppController {
    private static AppController INSTANCE = null;

    private static JarLoader jarLoader = new JarLoader();

    private static AppModel appModel;

    private static LoadImageListener loadImageListener = null;
    private static NewProjectListener newProjectListener = null;
    private static ProjectSwitchListener projectSwitchListener = null;
    private static SelectHistoryItemListener selectHistoryItemListener = null;
    private static RedoListener redoListener = null;
    private static UndoListener undoListener = null;
    private static CloseProjectListener closeProjectListener = null;
    private static SaveProjectListener saveProjectListener = null;
    private static SaveProjectAsListener saveProjectAsListener = null;
    private static OpenProjectListener openProjectListener = null;
    private static RenameListener renameListener = null;
    private static SaveImageListener saveImageListener = null;
    private static SaveImageAsListener saveImageAsListener = null;
    private static FilterListener filterListener = null;
    private static ExitListener exitListener = null;
    private static ChangeHistorySizeListener changeHistorySizeListener = null;
    private static ButtonListener buttonListener = null;
    private static SaveAllListener saveAllListener = null;

    public static AppController getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppController();
            String[] choices = Language.GetLanguagesList();
            String input = (String) JOptionPane.showInputDialog(null, "Language",
                    "Please choose a language", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
            Language.getInstance().setLanguage(input);
            appModel.setFilterList(jarLoader.getAllFilters().keySet().toArray(new String[jarLoader.getAllFilters().keySet().size()]));
        }
        return INSTANCE;
    }

    public void initView() {
        appModel.initView();
    }

    public LoadImageListener GetLoadImageListener() {
        if (loadImageListener == null)
            return new LoadImageListener(appModel);
        else
            return loadImageListener;
    }

    public NewProjectListener GetNewProjectListener() {
        if (newProjectListener == null)
            return new NewProjectListener(appModel);
        else
            return newProjectListener;
    }

    public ProjectSwitchListener GetProjectSwitchListener() {
        if (projectSwitchListener == null)
            return new ProjectSwitchListener(appModel);
        else
            return projectSwitchListener;
    }

    public SelectHistoryItemListener GetSelectHistoryItemListener() {
        if (selectHistoryItemListener == null)
            return new SelectHistoryItemListener(appModel);
        else
            return selectHistoryItemListener;
    }

    public RedoListener GetRedoListener() {
        if (redoListener == null)
            return new RedoListener(appModel);
        else
            return redoListener;
    }

    public UndoListener GetUndoListener() {
        if (undoListener == null)
            return new UndoListener(appModel);
        else
            return undoListener;
    }

    public CloseProjectListener GetCloseProjectListener() {
        if (closeProjectListener == null)
            return new CloseProjectListener(appModel);
        else
            return closeProjectListener;
    }

    public SaveProjectAsListener GetSaveProjectAsListener() {
        if (saveProjectAsListener == null)
            return new SaveProjectAsListener(appModel);
        else
            return saveProjectAsListener;
    }

    public SaveProjectListener GetSaveProjectListener() {
        if (saveProjectListener == null)
            return new SaveProjectListener(appModel);
        else
            return saveProjectListener;
    }

    public OpenProjectListener GetOpenProjectListener() {
        if (openProjectListener == null)
            return new OpenProjectListener(appModel);
        else
            return openProjectListener;
    }

    public RenameListener GetRenameListener() {
        if (renameListener == null)
            return new RenameListener(appModel);
        else
            return renameListener;
    }

    public SaveImageListener GetSaveImageListener() {
        if (saveImageListener == null)
            return new SaveImageListener(appModel);
        else
            return saveImageListener;
    }

    public SaveImageAsListener GetSaveImageAsListener() {
        if (saveImageAsListener == null)
            return new SaveImageAsListener(appModel);
        else
            return saveImageAsListener;
    }

    public FilterListener GetFilterListener() {
        if (filterListener == null)
            return new FilterListener(appModel);
        else
            return filterListener;
    }

    public ExitListener GetExitListener() {
        if (exitListener == null)
            return new ExitListener(appModel);
        else
            return exitListener;
    }

    public ChangeHistorySizeListener GetChangeHistorySizeListener() {
        if (changeHistorySizeListener == null)
            return new ChangeHistorySizeListener(appModel);
        else
            return changeHistorySizeListener;
    }

    public ButtonListener GetButtonListener() {
        if (buttonListener == null)
            return new ButtonListener(appModel);
        else
            return buttonListener;
    }

    public SaveAllListener GetSaveAllListener() {
        if (saveAllListener == null)
            return new SaveAllListener(appModel);
        else
            return saveAllListener;
    }


    public void setProjectName(String name) {
        appModel.getCurrentProject().setTitle(name);
    }

    public void addObserver(ProjectPanel projectPanel) {
        appModel.getCurrentProject().addObserver(projectPanel);
    }

    public void addObserver(MainWindowView mainWindow) {
        appModel.addObserver(mainWindow);
    }

    public void addObserver(HistoryPanelView historyPanelView) {
        appModel.getCurrentProject().getHistory().addObserver(historyPanelView);
        appModel.addObserver(historyPanelView);
    }

    public void addObserver(ButtonView buttonView) {
        appModel.addObserver(buttonView);
        appModel.getCurrentProject().addObserver(buttonView);
    }

    public void addObserver(MenuView menuView) {
        appModel.addObserver(menuView);
    }

    public Filter GetFilter(String name) {
        return jarLoader.getFilter(name);
    }

    private AppController() {
        appModel = AppModel.getInstance("test");
        openProjectListener = GetOpenProjectListener();
        projectSwitchListener = GetProjectSwitchListener();
        newProjectListener = GetNewProjectListener();
    }


}
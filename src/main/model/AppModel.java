package main.model;

import main.config.Language;
import main.model.history.HistoryModel;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/**
 * Created by root on 11/05/16.
 */
public class AppModel extends ModelObject {
    protected Language language = Language.getInstance();
    private static AppModel INSTANCE = null;

    private ArrayList<ProjectModel> projects = new ArrayList<>();
    private String[] filters;

    private int currentProject = 0;
    private String projectName = "UNTITLED";

    private String lastClicked;

    public AppModel(String title) {
        ProjectModel projectModel = new ProjectModel();
        projectModel.setHistory(new HistoryModel());
        projects.add(projectModel);
        projectName = title;
    }

    public AppModel() {
        ProjectModel projectModel = new ProjectModel();
        projectModel.setHistory(new HistoryModel());
        projects.add(projectModel);
        projectName = "UNTITLED";
    }

    public void initView() {
        setChanged();
        notifyObservers(filters);
    }

    public void renameCurrentProject(String name) {
        getCurrentProject().setTitle(name);
        update(getCurrentProject(), "RENAME");
    }

    public void historySelectItem(int index) {
        if (index >= 0) {
            getCurrentProject().getHistory().switchPosition(index);
            if (getCurrentProject().getHistory().getList().size() != 0) {
                BufferedImage image = getCurrentProject().getHistory().getHistoryItemByIndex(index).getImage().getImage();
                getCurrentProject().reloadImage(image);
            }
        }
    }

    public void update(ProjectModel projectModel, String state) {
        setChanged();
        notifyObservers(new StatedObject<ProjectModel>(projectModel, state));
    }

    public void update(HistoryModel historyModel, String state) {
        setChanged();
        notifyObservers(new StatedObject<HistoryModel>(historyModel, state));
    }

    public void update(AppModel appModel, String state) {
        setChanged();
        notifyObservers(new StatedObject<AppModel>(appModel, state));
    }

    public void update(Integer i) {
        setChanged();
        notifyObservers(i);
    }

    public void newProject() {
        currentProject = projects.size();
        ProjectModel projectModel = new ProjectModel();
        projectModel.setHistory(new HistoryModel());
        projects.add(projectModel);
        update(projectModel, "CREATE");
    }

    public void closeCurrentProject() {
        projects.remove(currentProject);
        update(this, "CLOSE");
        if (projects.size() == 0)
            newProject();
    }

    public void setCurrentProject(int index) {
        currentProject = index;
        update(getCurrentProject().getHistory(), "SWITCH");
    }

    public static AppModel getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppModel();
        }
        return INSTANCE;
    }

    public static AppModel getInstance(String title) {
        if (INSTANCE == null) {
            INSTANCE = new AppModel(title);
        }
        return INSTANCE;
    }

    public ProjectModel getCurrentProject() {
        return projects.get(currentProject);
    }

    public int getCurrentProjectId() {
        return currentProject;
    }

    public void SaveCurrentProjectAs(String location) {
        getCurrentProject().SaveProjectAs(location);
    }

    public void SaveCurrentProject() {
        getCurrentProject().SaveProject();
    }

    public void setFilterList(String[] filters) {
        this.filters = filters;
    }

    public void openProject(String path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        GZIPInputStream gzipis = null;
        try {
            gzipis = new GZIPInputStream(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectInputStream oisCompressed = null;
        try {
            oisCompressed = new ObjectInputStream(gzipis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProjectModel project = null;
        try {
            project = (ProjectModel) oisCompressed.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        projects.add(project);
        project.getHistory().update(project.getHistory(), "ADD");
        currentProject = projects.indexOf(project);
        project.getHistory().switchPosition(0);
        update(project, "OPEN");

    }

    public boolean areProjectsSaved() {
        for (ProjectModel projectModel : projects) {
            if (!projectModel.isSaved())
                return false;
        }
        return true;
    }

    public void saveAllProjects() {
        for (ProjectModel project : projects) {
            this.setCurrentProject(projects.indexOf(project));
            SaveCurrentProject();
        }
    }

    public String getLastClicked() {
        return lastClicked;
    }

    public void setLastClicked(String name) {
        lastClicked = name;
    }
}
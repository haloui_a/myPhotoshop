package main.view.history;

import main.config.Configuration;
import main.model.history.HistoryItem;
import main.model.history.HistoryModel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by root on 11/05/16.
 */
public class IconListRenderer extends DefaultListCellRenderer {
    private HistoryModel history;

    public IconListRenderer(HistoryModel history) {
        super();
        this.history = history;
    }

    @Override
    public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(
                list, value, index, isSelected, cellHasFocus);
        Icon icon = this.getIcon(list, value, index, isSelected, cellHasFocus);
        label.setHorizontalTextPosition(JLabel.RIGHT);
        label.setFont(Configuration.DEFAULT_FONT);
        label.setIcon(icon);
        return label;
    }

    public Icon getIcon(JList list, Object value, int index,
                        boolean isSelected, boolean cellHasFocus) {
        if (history != null) {
            HistoryItem historyItem = history.getHistoryItemByIndex(index);
            if (historyItem != null)
                return new ImageIcon(historyItem.getImage().getImage().getScaledInstance(64, 64, 0));
            else
                return null;
        } else
            return null;
    }

    public void setHistoryModel(HistoryModel history) {
        this.history = history;
    }
}

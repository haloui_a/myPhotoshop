package main.view.window;

import main.config.Configuration;
import main.controller.AppController;

import javax.swing.*;

/**
 * Created by root on 11/05/16.
 */
public class WindowView extends JFrame {

    protected AppController appController = AppController.getInstance();

    public WindowView() {
        super();
        this.setInitialParameters();
        this.setVisible(true);
    }

    public WindowView(String title) {
        super();
        this.setInitialParameters();
        this.setTitle(title);
        this.setVisible(true);
    }

    private void setInitialParameters() {
        this.setResizable(Configuration.RESIZABLE);
        this.setTitle(Configuration.TITLE);
        this.setSize(Configuration.INITIAL_WIDTH, Configuration.INTITIAL_HEIGHT);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    public void refresh() {
        this.repaint();
        this.revalidate();
    }

}

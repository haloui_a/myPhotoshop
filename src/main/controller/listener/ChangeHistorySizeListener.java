package main.controller.listener;

import main.model.AppModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 13/05/16.
 */
public class ChangeHistorySizeListener extends DefaultListener implements ActionListener {
    public ChangeHistorySizeListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] choices = {"1", "5", "10", "15", "20", "25", "50", "75", "100", "Limitless"};
        String input = (String) JOptionPane.showInputDialog(null, "Choose the new history size :", "History Size", JOptionPane.QUESTION_MESSAGE, null, choices, choices[9]);
        if (input == "Limitless")
            appModel.getCurrentProject().setHistorySize(-1);
        else
            appModel.getCurrentProject().setHistorySize(new Integer(input));
    }
}

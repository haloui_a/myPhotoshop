package main.controller.listener;

import main.model.AppModel;
import main.view.toolbar.ButtonView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 13/05/16.
 */
public class ButtonListener extends DefaultListener implements ActionListener {
    public ButtonListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String clicked = ((ButtonView) e.getSource()).getName();
        appModel.setLastClicked(clicked);
        switch (clicked) {
            case "UNDO":
                appModel.update(2);
                appModel.historySelectItem(Integer.max(0, appModel.getCurrentProject().getHistory().getCurrentPosition() - 1));
                appModel.update(1);
                break;
            case "REDO":
                appModel.update(2);
                appModel.historySelectItem(Integer.min(appModel.getCurrentProject().getHistory().getCurrentPosition() + 1, appModel.getCurrentProject().getHistory().getList().size() - 1));
                appModel.update(1);
                break;
            case "FILTER":
                appModel.update(1);
                appModel.getCurrentProject().stopThread();
            default:
                break;
        }
    }
}

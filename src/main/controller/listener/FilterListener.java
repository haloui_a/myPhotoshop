package main.controller.listener;

import main.controller.AppController;
import main.model.AppModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by root on 13/05/16.
 */
public class FilterListener extends DefaultListener implements ActionListener {
    private AppController appController = AppController.getInstance();

    public FilterListener(AppModel appModel) {
        super(appModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        appModel.setLastClicked("FILTER");
        appModel.update(2);


        appModel.getCurrentProject().applyFilter(appController.GetFilter(item.getText()));
    }
}

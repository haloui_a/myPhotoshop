package main.model.history;

import main.model.ModelObject;
import main.model.StatedObject;
import main.view.project.ImageView;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by root on 11/05/16.
 */
public class HistoryModel extends ModelObject implements Serializable {

    private int currentPosition;
    private int size = -1;

    public void setSize(int i) {
        size = i;
    }

    public HistoryModel() {
        super();
        update(this, "ADD");
    }

    public void load() {
        for (HistoryItem hist : historyElmts) {
            hist.load();
        }
    }

    public ArrayList<String> getList() {
        ArrayList<String> strList = new ArrayList<>();
        for (HistoryItem historyItem : historyElmts) {
            strList.add(historyItem.getDescription());
        }
        return strList;
    }

    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    private ArrayList<HistoryItem> historyElmts = new ArrayList<>();

    public void Add(String description, ImageView image) {
        ArrayList<Integer> indexToDelete = new ArrayList<>();
        for (int i = currentPosition + 1; i <= historyElmts.size() - 1; i++) {
            indexToDelete.add(i);
        }
        Collections.sort(indexToDelete);
        Collections.reverse(indexToDelete);
        for (int i : indexToDelete) {
            historyElmts.remove(i);
        }
        historyElmts.add(new HistoryItem(description, new ImageView(deepCopy(image.getImage()), description)));
        currentPosition = historyElmts.size() - 1;

        if (size != -1) {
            while (historyElmts.size() > size) {
                historyElmts.remove(0);
            }
        }

        update(this, "ADD");
    }

    public HistoryItem getHistoryItemByIndex(int index) {
        if (index >= 0 && index <= historyElmts.size() - 1)
            return historyElmts.get(index);
        else
            return null;
    }

    public void update(HistoryModel historyModel, String state) {
        setChanged();
        notifyObservers(new StatedObject<HistoryModel>(historyModel, state));
    }

    public void switchPosition(int index) {
        if (currentPosition != index) {
            currentPosition = index;
            update(this, "SWITCH");
        }
    }

    public int getCurrentPosition() {
        return currentPosition;
    }


}

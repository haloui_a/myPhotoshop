package filter.basic;

import filter.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class Invert implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        BufferedImage res = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++) {
                Color c = new Color(img.getRGB(i, j));
                Color newc = new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
                res.setRGB(i, j, newc.getRGB());
            }
        return res;
    }

    @Override
    public String getName()
    {
        return "Invert";
    }
}

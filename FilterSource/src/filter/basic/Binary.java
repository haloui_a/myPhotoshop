package filter.basic;

import filter.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class Binary implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        BufferedImage res = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++)
            {
                Color c = new Color(img.getRGB(i, j));
                int k = (c.getRed() + c.getBlue() + c.getGreen()) / 3;
                if (k > (255 / 2))
                    res.setRGB(i, j, 0xffffff);
                else
                    res.setRGB(i, j, 0x000000);
            }
        return res;
    }

    @Override
    public String getName()
    {
        return "Binary";
    }
}

package main.controller.listener;

import main.config.Language;
import main.model.AppModel;

/**
 * Created by root on 11/05/16.
 */
public class DefaultListener {
    protected AppModel appModel;
    protected Language language = Language.getInstance();

    public DefaultListener(AppModel appModel) {
        this.appModel = appModel;
    }
}

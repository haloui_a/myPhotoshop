package main.view.menu;

import javax.swing.*;

/**
 * Created by root on 11/05/16.
 */
public class TopMenuView extends JMenuBar {
    public TopMenuView() {
        super();
        this.add(new ProjectMenuView());
        this.add(new EditMenuView());
        this.add(new FilterMenuView());
    }
}

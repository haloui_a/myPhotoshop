package main.view.project;

import main.controller.AppController;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by root on 11/05/16.
 */
public class PanelView extends JPanel implements Observer {


    protected transient AppController appController = AppController.getInstance();

    public void update(Observable obs, Object obj) {

    }

    public PanelView() {
        super();
        this.setLayout(new BorderLayout());
    }
}
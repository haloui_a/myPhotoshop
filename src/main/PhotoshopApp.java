package main;

import main.view.project.ProjectPanel;
import main.view.window.MainWindowView;

/**
 * Created by root on 11/05/16.
 */
public class PhotoshopApp {
    public PhotoshopApp() {
        MainWindowView mainWindow = new MainWindowView("MyPhotoshop");

        mainWindow.addTab("UNTITLED", new ProjectPanel());
        mainWindow.refresh();
    }
}

package main.view.menu;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Created by root on 11/05/16.
 */
public class ProjectMenuView extends MenuView {

    public ProjectMenuView() {
        super();
        this.setText(language.GetTitle(language.TITLE_PROJECT));
        this.setMnemonic(KeyEvent.VK_P);

        JMenuItem menuSaveImageItem = new JMenuItem(language.GetTitle(language.TITLE_SAVE_IMAGE), KeyEvent.VK_I);
        menuSaveImageItem.addActionListener(appController.GetSaveImageListener());

        JMenuItem menuSaveImageAsItem = new JMenuItem(language.GetTitle(language.TITLE_SAVE_IMAGE_AS));
        menuSaveImageAsItem.addActionListener(appController.GetSaveImageAsListener());

        JMenuItem menuOpenImageItem = new JMenuItem(language.GetTitle(language.TITLE_LOAD_IMAGE), KeyEvent.VK_L);
        menuOpenImageItem.addActionListener(appController.GetLoadImageListener());

        JMenuItem menuCreateItem = new JMenuItem(language.GetTitle(language.TITLE_CREATE_PROJECT), KeyEvent.VK_C);
        menuCreateItem.addActionListener(appController.GetNewProjectListener());

        JMenuItem menuOpenItem = new JMenuItem(language.GetTitle(language.TITLE_OPEN_PROJECT), KeyEvent.VK_O);
        menuOpenItem.addActionListener(appController.GetOpenProjectListener());

        JMenuItem menuCloseItem = new JMenuItem(language.GetTitle(language.TITLE_CLOSE_PROJECT), KeyEvent.VK_C);
        menuCloseItem.addActionListener(appController.GetCloseProjectListener());

        JMenuItem menuSaveItem = new JMenuItem(language.GetTitle(language.TITLE_SAVE), KeyEvent.VK_S);
        menuSaveItem.addActionListener(appController.GetSaveProjectListener());

        JMenuItem menuSaveAsItem = new JMenuItem(language.GetTitle(language.TITLE_SAVE_AS));
        menuSaveAsItem.addActionListener(appController.GetSaveProjectAsListener());

        JMenuItem menuSaveAllItem = new JMenuItem(language.GetTitle(language.TITLE_SAVE_ALL));
        menuSaveAllItem.addActionListener(appController.GetSaveAllListener());

        JMenuItem menuExitItem = new JMenuItem(language.GetTitle(language.TITLE_EXIT), KeyEvent.VK_E);
        menuExitItem.addActionListener(appController.GetExitListener());

        this.add(menuSaveImageItem);
        this.add(menuSaveImageAsItem);
        this.add(menuOpenImageItem);
        this.addSeparator();
        this.add(menuCreateItem);
        this.add(menuOpenItem);
        this.add(menuCloseItem);
        this.addSeparator();
        this.add(menuSaveItem);
        this.add(menuSaveAsItem);
        this.add(menuSaveAllItem);
        this.addSeparator();
        this.add(menuExitItem);
    }
}

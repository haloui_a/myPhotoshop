package main.config;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by root on 12/05/16.
 */
public class Language {
    private static HashMap<String, HashMap<String, String>> languageStrings = new HashMap<>();

    public static String TITLE_IMAGE_LOADED = "Image loaded";
    public static String TITLE_PROJECT = "Project";
    public static String TITLE_EDIT = "Edit";
    public static String TITLE_FILTERS = "Filters";
    public static String TITLE_LOAD_PROJECT = "Load project";
    public static String TITLE_CREATE_PROJECT = "Create project";
    public static String TITLE_OPEN_PROJECT = "Open project";
    public static String TITLE_CLOSE_PROJECT = "Close project";
    public static String TITLE_LOAD_IMAGE = "Load image";
    public static String TITLE_SAVE_AS = "Save as";
    public static String TITLE_SAVE_ALL = "Save all";
    public static String TITLE_SAVE = "Save";
    public static String TITLE_EXIT = "Exit";
    public static String TITLE_UNDO = "Undo";
    public static String TITLE_REDO = "Redo";
    public static String TITLE_LOAD_NEW_FILTER = "Load new main.filter";
    public static String TITLE_RENAME_PROJECT = "Rename current project";
    public static String TITLE_TYPE_PROJECT_NAME = "Type a name for your project :";
    public static String TITLE_SAVE_IMAGE = "Save image";
    public static String TITLE_SAVE_IMAGE_AS = "Save image as...";

    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    private String currentLanguage;

    private static Language INSTANCE = null;

    public static Language getInstance() {
        if (INSTANCE == null) {
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            INSTANCE = new Language(s + "/languages.xml");
        }
        return INSTANCE;
    }

    public void setLanguage(String currentLanguage) {
        this.currentLanguage = currentLanguage;
    }

    public void Parse(Document document) {
        NodeList root = document.getChildNodes().item(0).getChildNodes();
        int nbRoots = root.getLength();

        for (int i = 0; i < nbRoots; i++) {
            if (root.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element lang = (Element) root.item(i);
                addNewLanguage(lang.getAttribute("LANGUAGE_CODE"));
                NodeList translations = lang.getChildNodes();
                int nbTranslations = translations.getLength();
                for (int j = 0; j < nbTranslations; j++) {
                    if (translations.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        final Element traduction = (Element) translations.item(j);
                        addNewTraductionFor(lang.getAttribute("LANGUAGE_CODE"), traduction.getAttribute("original"), traduction.getAttribute("translated"));
                    }
                }
            }
        }
    }

    public void CreateDefaultFile(DocumentBuilder builder) {

        addNewLanguage("EN");
        addNewLanguage("FR");
        addNewLanguage("ES");

        addNewTraductionFor("FR", TITLE_IMAGE_LOADED, "Image chargée");
        addNewTraductionFor("EN", TITLE_IMAGE_LOADED, "Image loaded");
        addNewTraductionFor("ES", TITLE_IMAGE_LOADED, "Imagen cargada");

        addNewTraductionFor("FR", TITLE_PROJECT, "Projet");
        addNewTraductionFor("EN", TITLE_PROJECT, "Project");
        addNewTraductionFor("ES", TITLE_PROJECT, "Proyecto");

        addNewTraductionFor("FR", TITLE_EDIT, "Modifier");
        addNewTraductionFor("EN", TITLE_EDIT, "Edit");
        addNewTraductionFor("ES", TITLE_EDIT, "Editar");

        addNewTraductionFor("FR", TITLE_FILTERS, "Filtres");
        addNewTraductionFor("EN", TITLE_FILTERS, "Filters");
        addNewTraductionFor("ES", TITLE_FILTERS, "Filtros");

        addNewTraductionFor("FR", TITLE_CREATE_PROJECT, "Créer un projet");
        addNewTraductionFor("EN", TITLE_CREATE_PROJECT, "Create new project");
        addNewTraductionFor("ES", TITLE_CREATE_PROJECT, "Crear nuevo proyecto");

        addNewTraductionFor("FR", TITLE_OPEN_PROJECT, "Ouvrir un projet existant");
        addNewTraductionFor("EN", TITLE_OPEN_PROJECT, "Open existing project");
        addNewTraductionFor("ES", TITLE_OPEN_PROJECT, "Abrir proyecto existente");

        addNewTraductionFor("FR", TITLE_CLOSE_PROJECT, "Fermer le projet courant");
        addNewTraductionFor("EN", TITLE_CLOSE_PROJECT, "Close current project");
        addNewTraductionFor("ES", TITLE_CLOSE_PROJECT, "Cerrar actual proyecto");

        addNewTraductionFor("FR", TITLE_EXIT, "Quitter");
        addNewTraductionFor("EN", TITLE_EXIT, "Exit");
        addNewTraductionFor("ES", TITLE_EXIT, "Salir");

        addNewTraductionFor("FR", TITLE_UNDO, "Annuler");
        addNewTraductionFor("EN", TITLE_UNDO, "Undo");
        addNewTraductionFor("ES", TITLE_UNDO, "Annular");

        addNewTraductionFor("FR", TITLE_REDO, "Refaire");
        addNewTraductionFor("EN", TITLE_REDO, "Redo");
        addNewTraductionFor("ES", TITLE_REDO, "Rehacer");

        addNewTraductionFor("FR", TITLE_LOAD_NEW_FILTER, "Charger un nouveau filtre");
        addNewTraductionFor("EN", TITLE_LOAD_NEW_FILTER, "Load new main.filter");
        addNewTraductionFor("ES", TITLE_LOAD_NEW_FILTER, "Cargar nuevo filtro");

        addNewTraductionFor("FR", TITLE_LOAD_IMAGE, "Charger une image");
        addNewTraductionFor("EN", TITLE_LOAD_IMAGE, "Load image");
        addNewTraductionFor("ES", TITLE_LOAD_IMAGE, "Cargar imagen");

        addNewTraductionFor("FR", TITLE_SAVE, "Sauvegarder");
        addNewTraductionFor("EN", TITLE_SAVE, "Save");
        addNewTraductionFor("ES", TITLE_SAVE, "Guardar");

        addNewTraductionFor("FR", TITLE_SAVE_ALL, "Sauvegarder tout");
        addNewTraductionFor("EN", TITLE_SAVE_ALL, "Save all");
        addNewTraductionFor("ES", TITLE_SAVE_ALL, "Guardar todos");

        addNewTraductionFor("FR", TITLE_SAVE_AS, "Sauvegarder comme...");
        addNewTraductionFor("EN", TITLE_SAVE_AS, "Save as...");
        addNewTraductionFor("ES", TITLE_SAVE_AS, "Guardar como...");

        addNewTraductionFor("FR", TITLE_RENAME_PROJECT, "Renommer le projet courant");
        addNewTraductionFor("EN", TITLE_RENAME_PROJECT, "Rename current project");
        addNewTraductionFor("ES", TITLE_RENAME_PROJECT, "Poner nuevo nombre a actual proyecto");

        addNewTraductionFor("FR", TITLE_TYPE_PROJECT_NAME, "Saisissez un nom pour le projet : ");
        addNewTraductionFor("EN", TITLE_TYPE_PROJECT_NAME, "Type a name for your project :");
        addNewTraductionFor("ES", TITLE_TYPE_PROJECT_NAME, "Escriba el nombre del proyecto :");

        addNewTraductionFor("FR", TITLE_SAVE_IMAGE, "Sauvegarder l'image");
        addNewTraductionFor("EN", TITLE_SAVE_IMAGE, "Save image");
        addNewTraductionFor("ES", TITLE_SAVE_IMAGE, "Guardar la imagen");

        addNewTraductionFor("FR", TITLE_SAVE_IMAGE_AS, "Sauvegarder l'image sous...");
        addNewTraductionFor("EN", TITLE_SAVE_IMAGE_AS, "Save image as...");
        addNewTraductionFor("ES", TITLE_SAVE_IMAGE_AS, "Guardar la imagen as...");

        final Document document = builder.newDocument();
        final Element root = document.createElement("languages");
        document.appendChild(root);

        for (String lang : languageStrings.keySet()) {
            final Element langItem = document.createElement("lang");
            langItem.setAttribute("LANGUAGE_CODE", lang);
            for (String original : languageStrings.get(lang).keySet()) {
                final Element traductionItem = document.createElement("traduction");
                traductionItem.setAttribute("original", original);
                traductionItem.setAttribute("translated", languageStrings.get(lang).get(original));
                langItem.appendChild(traductionItem);
            }
            root.appendChild(langItem);
        }


        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource source = new DOMSource(document);
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            final StreamResult out = new StreamResult(new File(s + "/languages.xml"));

            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, out);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public Language(String file) {

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File fileOut = new File(file);
            if (!fileOut.exists()) {
                CreateDefaultFile(builder);
                return;
            }
            Document document = builder.parse(fileOut);
            if (document.getDocumentElement().getNodeName() == "languages")
                Parse(document);
            else
                CreateDefaultFile(builder);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String GetTitle(String title) {
        return GetTitle(title, currentLanguage);
    }

    public void addNewLanguage(String name) {
        languageStrings.put(name, new HashMap<>());
    }

    public void addNewTraductionFor(String language, String title, String traduction) {
        languageStrings.get(language).put(title, traduction);
    }

    public static String[] GetLanguagesList() {
        Set<String> strings = languageStrings.keySet();
        ArrayList<String> strArray = new ArrayList<>();
        for (String str : strings) {
            strArray.add(str);
        }
        return strArray.toArray(new String[0]);
    }

    public static String GetTitle(String title, String language) {
        return languageStrings.get(language).get(title);
    }
}

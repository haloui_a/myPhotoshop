package main.filter;

import filter.Filter;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by root on 13/05/16.
 */
public class JarLoader {
    private HashMap<String, Filter> filters = new HashMap<>();

    public JarLoader() {
        loadFilters();
    }

    public Filter getFilter(String name) {
        return filters.get(name);
    }

    public HashMap<String, Filter> getAllFilters() {
        return filters;
    }

    public void loadFilters() {
        ArrayList<String> jarFiles = getAllJarPaths();
        for (String jarFile : jarFiles) {
            loadAllFromJAR(jarFile);
        }
    }

    public void loadAllFromJAR(String path) {
        JarFile jarFile = null;
        try {
            jarFile = new JarFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Enumeration<JarEntry> e = jarFile.entries();

        URL[] urls = new URL[0];
        try {
            urls = new URL[]{new URL("jar:file:" + path + "!/")};
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        URLClassLoader cl = URLClassLoader.newInstance(urls);

        while (e.hasMoreElements()) {
            JarEntry je = e.nextElement();
            if (je.isDirectory() || !je.getName().endsWith(".class")) {
                continue;
            }
            String className = je.getName().substring(0, je.getName().length() - 6);
            className = className.replace('/', '.');
            try {
                Class filterClass = cl.loadClass(className);
                if (!filterClass.getName().contains("Convolution")) {
                    Filter filter = (Filter) filterClass.newInstance();
                    filters.put(filter.getName(), filter);
                }
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }

        }
    }

    public ArrayList<String> getAllJarPaths() {
        ArrayList<String> jarFiles = new ArrayList<>();
        File folder = new File("./filter/");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(".jar")) {
                jarFiles.add(listOfFiles[i].getAbsolutePath());
            }
        }
        return jarFiles;
    }
}


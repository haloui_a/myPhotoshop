package filter.basic;

import filter.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by root on 13/05/16.
 */
public class RotateRight implements Filter {
    @Override
    public BufferedImage perform(BufferedImage img)
    {
        BufferedImage res = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());

        for (int i = 0; i < img.getWidth(); i++)
            for (int j = 0; j < img.getHeight(); j++) {
                int k = j + 1;
                res.setRGB(img.getHeight() - k, i, img.getRGB(i, j));
            }
        return res;
    }

    @Override
    public String getName()
    {
        return "RotateRight";
    }
}
